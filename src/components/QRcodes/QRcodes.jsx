import React from 'react';
import QRcode from 'react-qr-code';
import { Link } from "react-router-dom";

const QRcodes = () =>{
    return(
    <QRcode marginLeft={20} size={120} value='https://www.spotify.com/es/home/'/>
  )
}

export default QRcodes;