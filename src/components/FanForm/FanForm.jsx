import React from "react";
import { useState } from "react";
import { register } from "../../api/auth";
import Button from "../Button";
import { Link } from "react-router-dom";

import "../../styles/mainStyles.scss";
import "./FanForm.scss";

const Register = (props) => {
  const [INITIAL_STATE, setINITIAL_STATE] = useState({
    nameAndSurname: "",
    email: "",
    postalCod: "",
    password: "",
    cardNumber: "",
    date: "",
    cvv: "",
    titular: "",
  });

  async function handleSubmitForm(ev) {
    ev.preventDefault();
    const { history, saveUser } = props;
    try {
      const data = await register(INITIAL_STATE);
     
      setINITIAL_STATE(INITIAL_STATE);
      history.push("/");
    } catch (error) {
      setINITIAL_STATE({ error: error.message });
    }
  }

  function handleChangeInput(ev) {
    const name = ev.target.name;
    const value = ev.target.value;
    setINITIAL_STATE({
      ...INITIAL_STATE,
      [name]: value,
    });
  }

  return (
    <form className="form" onSubmit={handleSubmitForm}>
      <h3 className="form__titlefan">Datos Personales</h3>

      <label htmlFor="nameAndSurname">
        <input
          className="form__inputfan"
          type="text"
          name="nameAndSurname"
          placeholder="Nombre y apellidos"
          value={INITIAL_STATE.nameAndSurname}
          onChange={handleChangeInput}
        />
      </label>

      <label htmlFor="email">
        <input
          className="form__inputfan"
          type="text"
          name="email"
          placeholder="Email"
          value={INITIAL_STATE.email}
          onChange={handleChangeInput}
        />
      </label>

      <label htmlFor="postalCod">
        <input
          className="form__inputfan"
          type="text"
          name="postalCod"
          placeholder="Codigo Postal"
          value={INITIAL_STATE.postalCod}
          onChange={handleChangeInput}
        />
      </label>

      <h3 className="form__titlefan">Pago con tarjeta</h3>

      <label htmlFor="cardNumber">
        <input
          className="form__inputfan"
          type="text"
          name="cardNumber"
          placeholder="Numero de tarjeta"
          value={INITIAL_STATE.cardNumber}
          onChange={handleChangeInput}
        />
      </label>

      <div className="card">
        <label htmlFor="date">
          <input
            className="form__inputshort"
            type="text"
            name="date"
            placeholder="Mes/Año"
            value={INITIAL_STATE.date}
            onChange={handleChangeInput}
          />
        </label>

        <label htmlFor="cvv">
          <input
            className="form__inputshort"
            type="text"
            name="cvv"
            placeholder="CVV"
            value={INITIAL_STATE.cvv}
            onChange={handleChangeInput}
          />
        </label>
      </div>

      <label htmlFor="titular">
        <input
          className="form__inputfan"
          type="text"
          name="titular"
          placeholder="Titular"
          value={INITIAL_STATE.titular}
          onChange={handleChangeInput}
        />
      </label>

      <div>
        <Button style="fanClub" type="submit">
          Continuar
        </Button>
      </div>

      <Link className="form__text" to="/home">
        <p>Más tarde</p>
      </Link>
    </form>
  );
};

export default Register;
