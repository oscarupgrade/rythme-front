import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import {useDispatch} from "react-redux";
import {setUser} from "../../app/appSlice";
import { register } from '../../api/auth';
import Button from '../Button';

import '../../styles/mainStyles.scss';
import './Register.scss';

const INITIAL_STATE = {
    name: '',
    surname: '',
    // dataOfBirth: '',
    // img: '',
    email: '',
    password: '',
}
const Register = (props) => {
    const dispatch = useDispatch();
    const [state, setState] = useState(INITIAL_STATE);
    

    async function handleSubmitForm  (ev){
        ev.preventDefault();
        const { history, saveUser } = props;
        try {
            const data = await register(state);
            dispatch(setUser(data));
           
            setState(INITIAL_STATE);
            history.push('/home');
        } catch (error) {
            setState({...state, error: error.message});
        }
    };

    function handleChangeInput (ev){
        const name = ev.target.name;
        const value = ev.target.value;
        setState({
            ...state,
            [name]: value,
        })
    };
   
    return(
    
        <form className="form" onSubmit={handleSubmitForm}>
            <h1 className="form__title" >Register</h1>

            <label htmlFor="name">
                <input className="form__input" 
                type="text" name="name" 
                placeholder="Name" 
                value={state.name} 
                onChange={handleChangeInput}/>
            </label>

            <label  htmlFor="surname">
                <input className="form__input" 
                type="text" name="surname" 
                placeholder="Surname" 
                value={state.surname} 
                onChange={handleChangeInput}/>
            </label>

            {/* <label  htmlFor="dataOfBirth">
                <input className="form__input" 
                type="date" name="dataOfBirth" 
                placeholder="Date of Birth" 
                value={INITIAL_STATE.dataOfBirth} 
                onChange={handleChangeInput}/>
            </label> */}

            {/* <label  htmlFor="img">
                <input className="form__input--color" 
                type="file" name="img" 
                placeholder="Image" 
                value={INITIAL_STATE.img} 
                onChange={handleChangeInput}/>
            </label> */}

            <label htmlFor="email">
                <input className="form__input" 
                type="text" name="email" 
                placeholder="Email" 
                value={state.email} 
                onChange={handleChangeInput}/>
            </label>

            <label htmlFor="password">
                <input className="form__input" 
                type="password" name="password" 
                placeholder="Password" 
                value={state.password} 
                onChange={handleChangeInput}/>
            </label>

            <div>
                <Button style="fanClub" type="submit">Check in</Button>
            </div>

            <Link className="form__text" to="/login">
                <p>I'm already registered</p>
            </Link>
        </form>
    )
}

export default Register;