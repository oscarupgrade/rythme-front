import React, { useState } from "react";

import useChat from '../useChat';
import MessageCard from "../MessageCard";
import SendMessage from "../SendMessage";

import './ChatFriendList.scss';

const ChatFriendList = (props) => {
    const room = props.channel;
    const msg = props.messages;
    const { messages, sendMessage } = useChat(room, msg);
    const [newMessage, setNewMessage] = useState('');

    const handleSendMessage = (ev) => {
        ev.preventDefault();
        sendMessage(newMessage);
        setNewMessage("");
    };

    const handleNewMessageChange = (ev) => {
        setNewMessage(ev.target.value);
    };

    return(
        <div className={'chat'}>

            <ol className="chat__messages">
                <div className={'chat__messages'}>
                    {messages.map((message, i) => {
                        return(<MessageCard message={message} key={i}/>)
                    })}
                </div>
            </ol>
            <div className={'chat__form'}>
                <SendMessage
                    onChange={handleNewMessageChange}
                    value={newMessage}
                    submit={handleSendMessage}
                />
            </div>
        </div>
    );
};

export default ChatFriendList;