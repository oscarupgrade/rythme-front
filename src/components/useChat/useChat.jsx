import React,{ useEffect, useRef, useState } from "react";
import socketIOClient from "socket.io-client"
import { useSelector } from "react-redux";
import { selectUser } from "../../app/appSlice";

const NEW_CHAT_MESSAGE_EVENT = "newChatMessage";
const SOCKET_SERVER_URL = `https://rythme-project.herokuapp.com:3250`;

const useChat = (room, msg) => {
    const user = useSelector(selectUser);
    const [messages, setMessages] = useState(msg);
    const socketRef = useRef();
    useEffect(()=>{
        socketRef.current = socketIOClient(SOCKET_SERVER_URL, {
            query: { room },
        });

        socketRef.current.on(NEW_CHAT_MESSAGE_EVENT, (message) => {
            const incomingMessage = {
                ...message,
                ownedByCurrentUser: String(message.senderId) === String(user?.data?._id),
            };
            setMessages((messages) => [...messages, incomingMessage]);
        });

        return () => {
            socketRef.current.disconnect();
        };
    },[room]);

    const sendMessage = (messageBody) => {
        const now = new Date();
        socketRef.current.emit(NEW_CHAT_MESSAGE_EVENT, {
            body: messageBody,
            senderId: user.data._id,
            date: now
            // senderId: socketRef.current.id,
        });
    };

    return { messages, sendMessage };
};
export default useChat;