import './MessageCard.scss';

const MessageCard = (props) => {
    return(
      <div className={props.message.ownedByCurrentUser ? 'message' : 'message--receive'}>
            <span>{props.message.body}</span>
      </div>
    );
};

export default MessageCard;