import React from 'react';
import TicketCard from '../TicketCard';

import '../../styles/mainStyles.scss';
import './TicketList.scss';

const TicketList = (props) => {
    return(
        <div className="event">
            <p className="event__date">{props.date}</p>
            {props.tickets.map(ticket => {
                return(
                    <TicketCard ticket={ticket} key={ticket._id} />
                    )
                })
            }
            <hr className="line"></hr>
        </div>
    )};

export default TicketList;