import React, { useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  selectLocation,
  selectUser,
  setLocationPage,
} from "../../app/appSlice";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faStar,
  faTicketAlt,
  faComment,
  faMusic,
  faUser,
  faCog,
} from "@fortawesome/free-solid-svg-icons";

import "../../styles/mainStyles.scss";
import "./MainMenu.scss";

const MainMenu = (props) => {
  const refTickets = useRef();
  const refChat= useRef();
  const refHome = useRef();
  const refConcerts = useRef();
  const refProfile = useRef();
  const refBorder = useRef();
  const refMenu = useRef();
  const refLogin = useRef();
  const dispatch = useDispatch();
  const position = useSelector(selectLocation);
  const user = useSelector(selectUser);

  useEffect(() => {
    const eventResize = window.addEventListener('resize', ()=>getClass(position));
    return(window.removeEventListener('resize', eventResize));
  },[]);

  const getClass = (pos) => {
    let offsetActiveItem = "";
    if (pos === "tickets") {
      refTickets?.current?.classList?.add("active");
      offsetActiveItem = refTickets?.current?.getBoundingClientRect();
    } else {
      refTickets?.current?.classList?.remove("active");
    }
    if (pos === "chat") {
      refChat?.current?.classList?.add("active");
      offsetActiveItem = refChat?.current?.getBoundingClientRect();
    } else {
      refChat?.current?.classList?.remove("active");
    }
    if (pos === "home") {
      refHome?.current.classList.add("active");
      offsetActiveItem = refHome?.current.getBoundingClientRect();
    } else {
      refHome?.current.classList.remove("active");
    }
    if (pos === "concerts") {
      refConcerts?.current.classList.add("active");
      offsetActiveItem = refConcerts?.current.getBoundingClientRect();
    } else {
      refConcerts?.current.classList.remove("active");
    }
    if (pos === "profile" && user !== null) {
      refProfile?.current?.classList.add("active");
      offsetActiveItem = refProfile?.current?.getBoundingClientRect();
    } else if (user !== null) {
      refProfile?.current?.classList.remove("active");
    }
    if (pos === "login") {
      refLogin.current?.classList.add("active");
      offsetActiveItem = refLogin?.current?.getBoundingClientRect();
    } else if (user?.data === null) {
      refLogin.current?.classList.remove("active");
    }

    const left =
      Math.floor(
        offsetActiveItem.left -
          refMenu.current.offsetLeft -
          (refBorder.current.offsetWidth - offsetActiveItem.width) / 2
      ) + "px";
    refBorder.current.style.transform = `translate3d(${left}, 0 , 0)`;
  };

  const handleToggle = (ev) => {
    const pos = String(ev.target.id);
    dispatch(setLocationPage(pos));
    getClass(pos);
    props.history.push(`/${pos}`)
  };

  useEffect(() => {
    getClass(position);
  }, [position]);

  return (
    <div className="container">
      <div ref={refMenu} className="main_menu">
        <div
          onClick={handleToggle}
          id="tickets"
          ref={refTickets}
          className={`main_menu__element`}
        >
            <span className="main_menu__icon">
              <FontAwesomeIcon icon={faTicketAlt} />
            </span>
        </div>
        <div
          ref={refChat}
          id="chat"
          onClick={handleToggle}
          className={`main_menu__element`}
        >
            <span className="main_menu__icon">
              <FontAwesomeIcon icon={faComment} />
            </span>
        </div>
        <div
          ref={refHome}
          id="home"
          onClick={handleToggle}
          className={`main_menu__element`}
        >
            <span className="main_menu__icon">
              <FontAwesomeIcon icon={faStar} />
            </span>
        </div>
        <div
          ref={refConcerts}
          id="concerts"
          onClick={handleToggle}
          className={`main_menu__element`}
        >
            <span className="main_menu__icon">
              <FontAwesomeIcon icon={faMusic} />
            </span>
        </div>

        {user?.data && (
          <div
            ref={refProfile}
            id="profile"
            onClick={handleToggle}
            className={`main_menu__element`}
          >
              <span className="main_menu__icon">
                <FontAwesomeIcon icon={faCog} />
              </span>
          </div>
        )}

        {!user?.data && (
          <div
            ref={refLogin}
            id="login"
            onClick={handleToggle}
            className={`main_menu__element`}
          >
              <span className="main_menu__icon">
                <FontAwesomeIcon icon={faUser} />
              </span>
          </div>
        )}

        <div ref={refBorder} className="main_menu__border"></div>
      </div>

      <div className="svg-container">
        <svg viewBox="0 0 202.9 45.5">
          <clipPath
            id="menu"
            clipPathUnits="objectBoundingBox"
            transform="scale(0.0049285362247413 0.021978021978022)"
          >
            <path
              d="M6.7,45.5c5.7,0.1,14.1-0.4,23.3-4c5.7-2.3,9.9-5,18.1-10.5c10.7-7.1,11.8-9.2,20.6-14.3c5-2.9,9.2-5.2,15.2-7
          c7.1-2.1,13.3-2.3,17.6-2.1c4.2-0.2,10.5,0.1,17.6,2.1c6.1,1.8,10.2,4.1,15.2,7c8.8,5,9.9,7.1,20.6,14.3c8.3,5.5,12.4,8.2,18.1,10.5
          c9.2,3.6,17.6,4.2,23.3,4H6.7z"
            />
          </clipPath>
        </svg>
      </div>
    </div>
  );
};

export default withRouter(MainMenu);
