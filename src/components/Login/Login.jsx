import React, { useState } from 'react';
import { useDispatch } from "react-redux";
import { Link } from 'react-router-dom';
import {setLocationPage, setUser} from "../../app/appSlice";
import { login } from '../../api/auth';
import Button from '../Button';


import '../../styles/mainStyles.scss';
import './Login.scss';

const Login = (props) =>{
    const dispatch = useDispatch();
    const [INITIAL_STATE, setINITIAL_STATE] = useState({
        email: '',
        password: '',
    });

    async function handleSubmitForm  (ev){
        ev.preventDefault();
        const { history } = props;
        try {
            const user = await login(INITIAL_STATE);
            dispatch(setUser(user));
            dispatch(setLocationPage('home'));
            history.push('/home');
        } catch (error) {
            setINITIAL_STATE({error: error.message});
        }
    };

    function handleChangeInput (ev){
        const name = ev.target.name;
        const value = ev.target.value;
        setINITIAL_STATE({
            ...INITIAL_STATE,
            [name]: value,
        })
    };

    return(
       
        
          <form className="form" onSubmit={handleSubmitForm}>
            <h1 className="form__title" >Login</h1>

            <label htmlFor="email">
                <input className="form__input" 
                type="text" name="email" 
                placeholder="Email" 
                value={INITIAL_STATE.email} 
                onChange={handleChangeInput} 
                />
            </label>

            <label htmlFor="password">
                <input className="form__input" 
                type="password" name="password" 
                placeholder="Password"
                value={INITIAL_STATE.password} 
                onChange={handleChangeInput} 
                />
            </label>

            <div>
                <Button style="fanClub" type="submit">Login</Button>
            </div>

            <Link className="form__text" to="/register">
                <p>Not registered? Sign up</p>
            </Link>
        </form>
    )
}

export default Login;