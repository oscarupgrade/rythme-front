import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEuroSign } from "@fortawesome/free-solid-svg-icons";
import { faClock, faPlayCircle } from "@fortawesome/free-regular-svg-icons";

import "../../styles/mainStyles.scss";
import "./TicketCardBig.scss";

const TicketCardBig = (props) => {
  const { ticket } = props;
  return (
    <div className="box">
      <img src={ticket?.singer?.image} />
      <div className="box__detail">
        <div className="box__detail__singer">
          <h2 className="titles-header__name">{ticket?.singer?.name}</h2>
        </div>

        <div className="box__detail__date ">
          <p className="titles-header__card--size">{ticket?.date}</p>
        </div>

        <div className="box__detail__position">
          <FontAwesomeIcon icon={faClock}></FontAwesomeIcon>
          <span className="box__detail__time titles-header__card">
            {ticket?.time}
          </span>
        </div>

        <div>
          <FontAwesomeIcon icon={faEuroSign}></FontAwesomeIcon>
          <span className="box__detail__price titles-header__card">
            {ticket?.price}
          </span>
          {/* <FontAwesomeIcon
            className="box__detail__play"
            icon={faPlayCircle}
          ></FontAwesomeIcon> */}
        </div>
      </div>
    </div>
  );
};

export default TicketCardBig;
