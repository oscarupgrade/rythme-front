import React from "react";
import { useSelector } from "react-redux";
import { selectLocation } from "../../app/appSlice";


import "./Header.scss";
import "../../styles/mainStyles.scss";

const Header = () => {
  const header = useSelector(selectLocation);
  return (
      <div className="header">
        <h1 className="titles-header">{header.toUpperCase()}</h1>
      </div>
  );
};

export default Header;
