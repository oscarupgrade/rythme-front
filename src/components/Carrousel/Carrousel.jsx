import { Swiper, SwiperSlide } from "swiper/react";

import 'swiper/swiper.scss';
import './Carrousel.scss';
import '../../styles/mainStyles.scss'

const Carrousel = (props) => {
    return (
        <div className='carrousel'>
            <h3 className='carrousel__titles'>{props.title}</h3>
            <Swiper
                spaceBetween={10}
                slidesPerView={2}
                scrollbar={{ draggable: true }}
            >
                {
                    props.data.map(element => {
                        return(
                            <>
                                {props.concert ? (
                                    <SwiperSlide key={element._id}>
                                        <div className='carrousel__img'>
                                            <img src={element.singer.image}/>
                                        </div>
                                    </SwiperSlide>
                                ) : (
                                    <SwiperSlide key={element._id+element.name}>
                                        <div className='carrousel__img'>
                                            <img src={element.image}/>
                                        </div>
                                    </SwiperSlide>
                                )}

                            </>
                        )
                    })
                }
            </Swiper>
        </div>
    );
};

export default Carrousel;