import React, { useEffect, useState } from "react";
import ChatCard from "../ChatCard";

const ChatList = () => {
    const [chats, setChats] = useState([]);
    useEffect(() => {
        getChats();
    },[]);

    const getChats = async () => {
       const req = await fetch(`https://rythme-project.herokuapp.com/chat`, {
           method :"GET",
           headers: {
               "Accept": "application/json",
               "Content-Type": "application/json",
               "Access-Control-Allow-Credentials": true,
               "Access-Control-Allow-Origin": "*",
           },
           credentials: 'include'
       });
       const res = await req.json();
       setChats(res.data);
    };
    return(
        <div>
            { chats.map( chat => {
                return(<ChatCard chat={chat} key={chat._id} />);
            })
            }
        </div>
    );
};

export default ChatList;