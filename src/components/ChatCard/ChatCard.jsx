import {useSelector} from "react-redux";
import {selectUser} from "../../app/appSlice";
import './ChatCard.scss';
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";

const ChatCard = (props) => {
    const user = useSelector(selectUser);
    const [friend, setFriend] = useState({});

    useEffect(() => {
        if (props.chat.users[0]._id === user.data._id){
            setFriend({
                    name: props.chat.users[1].name,
                    image: props.chat.users[1].img,
                    msgDate: props.chat.messages[1]?.date,
                    lastMsg: props.chat.messages[1]?.body
                });
        } else {
            setFriend({
                    name: props.chat.users[0].name,
                    image: props.chat.users[0].img,
                    msgDate: props.chat.messages[props.chat.messages.length - 1]?.date,
                    lastMsg: props.chat.messages[props.chat.messages.length - 1]?.body
                })
        }
    },[]);

  return(
    <Link to={`/room-chat/${props.chat._id}`}><div className={'chat-card'}>
        <div className={'chat-card__img'}>
            {!friend.image && <img src={'https://www.pngkey.com/png/detail/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png'}/>}
            {friend.image && <img src={friend.image}/>}
        </div>
        <div className={'chat-card__info'}>
            <div className={'chat-card__info__detail'}>
                <span className={'chat-card__info__detail__user'}>{friend.name}</span><span className={'chat__info__detail__date'}>{friend.msgDate}</span>
            </div>
            <div className={'chat-card__info__message'}>
                <p>{friend.lastMsg}</p>
            </div>
        </div>
    </div> </Link>
  );
};

export default ChatCard;