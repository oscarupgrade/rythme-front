import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'; 
import { Link } from 'react-router-dom';


import './ConfigurationProfile.scss';


const ConfigurationProfile = () => {
    return(
        <div className='configuration'>
            {/* <Link to='/profile/edit'>
            <button className='configuration__button'>Edit profile 
            <FontAwesomeIcon className='configuration__icon1' 
            icon={faChevronRight}/>
            </button>
            </Link> */}

            {/* <button className='configuration__button'>Notifications
            <FontAwesomeIcon  className='configuration__icon2' 
            icon={faChevronRight}/></button> */}

            <Link to='/sync/platforms' >
            <button className='configuration__button'>Sync your music 
            <FontAwesomeIcon  className='configuration__icon3' 
            icon={faChevronRight}/></button>
            </Link>

            <button className='configuration__button'>Contacts 
            <FontAwesomeIcon  className='configuration__icon5' 
            icon={faChevronRight}/></button>

            {/* <button className='configuration__button'>History
            <FontAwesomeIcon  className='configuration__icon4' 
            icon={faChevronRight}/></button> */}
        </div>
    )
}

export default ConfigurationProfile;