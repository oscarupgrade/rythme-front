import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../Button';

import '../../styles/mainStyles.scss';
import './FinishBuy.scss';
import {useSelector} from "react-redux";
import {selectUser} from "../../app/appSlice";


const FinishBuy = () => {
    const user = useSelector(selectUser);
    return(
        <div className="finishbuy">
            <h1 className="finishbuy__title">¡Genial {user.data && user.data.name}!</h1>
            <p className="finishbuy__text">Que disfrutes del concierto.</p>
            <p className="finishbuy__text">RYTHME te ha enviado un correo <br/>
                con tu entrada y comprobante.
            </p>
            <Link className="finishbuy__link" to="/ticket/report">
                <Button style="fanClub">See my Ticket</Button>
            </Link>
        </div>
    )
}

export default FinishBuy;