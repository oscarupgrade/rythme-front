import React from "react";
import GoBack from "../GoBack";
import { Link } from 'react-router-dom';

import "./Platforms.scss";

const Platforms = () => {
  return (
    <div className="platforms">
      <GoBack />
      <p className="platforms__text">SINCRONIZA TU MÚSICA Y PLATAFORMAS</p>
    
    
      <div className="platforms__position">
        <div className="platforms__box1">
        <Link to='/sync/platforms/music'>
          <img src="/img/spotify.png" />
          </Link>
        </div>
    

        <div className="platforms__box1">
          <img src="/img/youtube.jpeg" />
        </div>
      </div>

      <div className="platforms__position">
        <div className="platforms__box1">
          <img src="/img/GooglePlay.png" />
        </div>

        <div className="platforms__box1">
          <img src="/img/soundcloud.jpg" />
        </div>
      </div>

      <div className="platforms__position">
        <div className="platforms__box1">
          <img src="/img/amazon.png" />
        </div>

        <div className="platforms__box1">
          <img src="/img/deezer.jpg" />
        </div>
      </div>
    </div>
  );
};

export default Platforms;
