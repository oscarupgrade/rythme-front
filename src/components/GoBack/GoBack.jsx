import React from "react";
import { withRouter } from "react-router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

import "./GoBack.scss";

const GoBack = (props) => {
  return (
    <withRouter>
      <FontAwesomeIcon 
        className="icon"
        icon={faChevronLeft}
        onClick={() => {
          props.history.goBack();
        }}
      ></FontAwesomeIcon>
    </withRouter>
  );
};

export default withRouter(GoBack);
