import React, { useState } from 'react';
import { withRouter } from "react-router-dom";
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { selectUser, setLocationPage, setTicket } from "../../app/appSlice";
import { wishPost } from '../../api/wishList';
import Button from '../Button';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

import '../../styles/mainStyles.scss';
import './TicketCard.scss';

const TicketCard = (props) => {
    const [active, setActive] = useState(false)
    const dispatch = useDispatch();
    const user = useSelector(selectUser);
    const { singer, room, price, assistants } = props.ticket;

    const saveTicket = () => {
        dispatch(setTicket(props.ticket));
    };

    const hasUser = () => {
        if (user?.data) {
            props.history.push('/ticket/buy');
        } else {
            dispatch(setLocationPage('login'));
            props.history.push('/login');
        }
    };

    const hasUsers = () => {
        if (user?.data) {
            props.history.push('/tickets');
        } else {
            dispatch(setLocationPage('login'));
            props.history.push('/login');
        }
    };

    async function submitForm (ev) {
        ev.preventDefault();
        try {
            await wishPost({eventId: props.ticket._id})
        } catch (error) {
            console.log(error.message)
        }
    }

    return(
            <div className='ticket' onClick={saveTicket}>
                <div className='ticket__col1'>
                    <Link to='/ticket/detail'>
                        <img src={singer.image}/>
                    </Link>
                </div>
                <div className='ticket__col2'>
                    <div className="ticket__location">
                        <p>{room.name}</p>
                    </div>

                    <form onSubmit={submitForm}>
                    <div className="ticket__location__wish">
                        <button onClick={hasUsers} className="ticket__location__wish--button" type='submit'>
                            <FontAwesomeIcon 
                            className={active ? 'wish' : 'wishTwo'} 
                            onClick={() => setActive(!active)} 
                            icon={faStar} />
                        </button>
                    </div>
                    </form>

                    <div>
                        <h3 className="ticket__singer">{singer.name}</h3>
                    </div>
                    <div className="ticket__assistants">
                        <p>{assistants} will attend</p>
                    </div>
                </div>
                <div className='ticket__col3'>
                    <div className='ticket__row'></div>
                    <div className="ticket__buy">
                        <Button onClick={hasUser} style="buy">Buy</Button>
                    </div>
                    <div className="ticket__price">
                        <p>{price}€</p>
                    </div>
                </div>
            </div>
    )
};


export default withRouter(TicketCard);