import './FriendCard.scss';

const FriendCard = (props) => {
    return(
      <div className={'friend'}>
          <div className={'friend__img'}>
              <img src={props.friend.image} />
          </div>
          <div className={'friend__info'}>
              <h3>{props.friend.name}</h3>
          </div>
      </div>
    );
};

export default FriendCard;