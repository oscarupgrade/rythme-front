import '../../styles/mainStyles.scss'

const getClasses = style => {
    if (style === 'medium') return 'button--sizemedium';
    if (style === 'medium-left') return 'button--sizemedium--radiusleft';
    if (style === 'medium-right') return 'button--sizemedium--radiusright';
    if (style === 'big') return 'button--sizebig';
    if (style === 'big-color') return 'button--sizebig--color'
    if (style === 'color') return 'button--color';
    if (style === 'share') return 'button__share';
    if (style === 'buy') return 'button__buy';
    if (style === 'buy-yellow') return 'button__buy--detail';
    if (style === 'fanClub') return 'button__fanClub';
    return 'button';
};

// Documentacion
// Props:
// onClick : function. Para enviar la funcion a realizar
// disable : boolean. Si esta habilitado o no el boton
// style : string. Le pasa el nombre de la clase

const Button = props => {
    return(
        <button onClick={props.onClick}
                disable={props.disable}
                className={`${getClasses(props.style)}`}
        >
            {props.children}
        </button>
    )
};

export default Button;