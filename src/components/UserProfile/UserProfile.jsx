import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {selectUser} from "../../app/appSlice";

import './UserProfile.scss';

const UserProfile = () => {
    const dispatch = useDispatch();
    const user = useSelector(selectUser);
    const [image, setImage] = useState('https://eshendetesia.com/images/user-profile.png');

    const uploadImage = async (e) => {
        const files = e.target.files;
        const data = new FormData();
        data.append("file", files[0]);
        data.append("upload_preset", "ml_default");

        const res = await fetch("https://api.cloudinary.com/v1_1/dj0zfm4sd/image/upload",
                                { method: "POST", body: data } );

        const file = await res.json();
        setImage(file.secure_url);

        // dispatch(setUser({}))
        await saveTicket(file.secure_url);

    };

    const saveTicket = async (img) => {
        try {
            await fetch(`https://rythme-project.herokuapp.com/users/save_profile`, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                },
                credentials: "include",
                body: JSON.stringify({img}),
            });

        } catch (err) {
            console.log(err);
        }
    };

    return(
        <div className={'user-profile'}>
            <div className={'user-profile__img'}>
                <form onChange={uploadImage}>
                    <label htmlFor={'input'}>
                        {user?.data?.img ? (<img src={user?.data?.img} />) : (<img src={image} />) }
                        <input type='file' id='input' className={'hidden'}/>
                    </label>
                </form>
            </div>
        </div>
    )
};

export default UserProfile;