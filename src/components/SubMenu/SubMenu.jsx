import React from "react";
import {useDispatch} from "react-redux";
import { Link } from "react-router-dom";
import {setLocationPage} from "../../app/appSlice";

import Button from '../Button';
import './SubMenu.scss';

const SubMenu = () => {
    const dispatch = useDispatch();
    return(
        <div className="submenu">
            <Link onClick={() => dispatch(setLocationPage('concerts'))} className='fanclub' to='/concerts'>
                <Button style="">Concerts</Button>
            </Link>
            <Link onClick={() => dispatch(setLocationPage('chat'))} className='fanclub' to='/chat'>
                <Button style="">Friends</Button>
            </Link>
            <Link className='fanclub' to='/fans'>
                <Button style="">Fan Club</Button>
            </Link>
        </div>
    )
}


export default SubMenu;