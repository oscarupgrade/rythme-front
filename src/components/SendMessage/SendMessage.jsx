import Button from "../Button";

import './SendMessage.scss';

const SendMessage = (props) => {
    return(
      <form className={'chat-form'}>
          <textarea onChange={props.onChange} className={'chat-form__text'} placeholder={'Message'} value={props.value}/>
          <Button onClick={props.submit} type={'submit'}>Send</Button>
      </form>
    );
};

export default SendMessage;