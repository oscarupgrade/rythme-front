import React from "react";
import GoogleMaps from "simple-react-google-maps";
// import { GoogleMap, useJsApiLoader } from '@react-google-maps/api';

import "./Maps.scss";

const Maps = () => {
  return (
    <GoogleMaps
      apiKey={"AIzaSyDvk2Puz2H9oDnaMCMBMr3L0zy_ZAFp3M4"}
      style={{
        height: "135px",
        width: "100%",
        marginTop: "20px",
        marginBottom: "10px",
      }}
      zoom={6}
      center={{ lat: 37.4224764, lng: -122.0842499 }}
      markers={[
        { lat: 37.4224764, lng: -122.0842499 },
        { lat: 37.5224764, lng: -121.0842499 },
        { lat: 37.3224764, lng: -120.0842499 },
      ]}
    />
  );
};

export default Maps;
