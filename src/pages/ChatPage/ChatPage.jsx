import ChatList from "../../components/ChatList";

import './ChatPage.scss';
import {useDispatch, useSelector} from "react-redux";
import {selectUser, setLocationPage} from "../../app/appSlice";
import FriendList from "../../components/FriendList";

const ChatPage = (props) => {
    const user = useSelector(selectUser);
    const dispatch = useDispatch();

    if (user?.data === null) {
        dispatch(setLocationPage('login'))
        props.history.push('/login');

    }
    return(
      <div className={'chat-page'}>
        <ChatList />
        {/*<FriendList />*/}
      </div>
  )
};

export default ChatPage;