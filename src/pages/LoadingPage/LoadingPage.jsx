import React from "react";
import Lottie from 'lottie-react-web'
import lf30_editor_jx3mtlqt from '../../animation/lf30_editor_jx3mtlqt.json';

import "./LoadingPage.scss";

const LoadingPage = (props) => {
  const { history } = props;
    const loading = () =>{
      history.push('/home');
    }

    setTimeout(loading, 3000);

    return(
        <div className='animation' >
          <div className='animation__title'>
            <h1>RythMe</h1>
          </div>
         <div className='animation__clip'>
           <Lottie options={{ animationData: lf30_editor_jx3mtlqt}} />
         </div>
        </div>
    )
}
export default LoadingPage;
