import SubMenu from '../../components/SubMenu';
import Carrousel from "../../components/Carrousel";
import './HomePage.scss';
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {selectUser} from "../../app/appSlice";

const HomePage = () => {
    const user = useSelector(selectUser);
    const [singers, setSingers] = useState([]);
    const [styles, setStyles] = useState([]);
    const [events, setEvents] = useState([]);

    useEffect(() => {
        getSingers(`https://rythme-project.herokuapp.com:3200/singers/search?order=0&start=1&end=9`);
        getStyles(`https://rythme-project.herokuapp.com:3200/singers/styles`);
        getEvents(`https://rythme-project.herokuapp.com:3200/events`);
    },[]);

    const getSingers = async (URL) => {
      const req = await fetch(URL);
      const res = await req.json();
      setSingers(res.result);
    };

    const getStyles = async (URL) => {
        const req = await fetch(URL);
        const res = await req.json();
        setStyles(res);
    };

    const getEvents = async (URL) => {
        const req = await fetch(URL);
        const res = await req.json();
        setEvents(res);
    };

    return(
        <div className='home'>
            <SubMenu />
            {user?.data && <h1 className='home__hello'>¡Hola {user.data.name}!</h1>}
            <Carrousel data={singers} title={'Singers'}/>
            <Carrousel data={styles} title={'Styles'}/>
            <Carrousel concert={true} data={events} title={'Concerts'}/>
        </div>
        
    );
};

export default HomePage;