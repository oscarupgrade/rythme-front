import React, {useEffect, useState} from 'react';
import TicketList from '../../components/TicketList';
import SubMenu from "../../components/SubMenu";

import './TicketsPage.scss';

const TicketsPage = () => {
    const [events, setEvents] = useState({});

    useEffect(() => {
        getEvents();
    },[]);

    const getEvents = async () => {
        const req = await fetch(`https://rythme-project.herokuapp.com/events/by-date`);
        const res = await req.json();
        setEvents(res);
        
    };

    return(
        <div className='events'>
            {/*<SubMenu />*/}
            { Object.keys(events).map(date => {
                    return(<TicketList  date={date} tickets={events[date]} key={date} />)
                })
            }  
        </div>
    )
};

export default TicketsPage;