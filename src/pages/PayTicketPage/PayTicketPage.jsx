import React, { useState } from "react";
import GoBack from "../../components/GoBack";
import { withRouter } from "react-router";
import { useSelector } from "react-redux";
import { selectTicket } from "../../app/appSlice";

import Button from "../../components/Button";

import "./PayTicketPage.scss";

const tax = 0.9;
let numTickets = 0;

const PayTicketPage = (props) => {
  const ticket = useSelector(selectTicket);
  const [totalPrice, setTotalPrice] = useState(ticket.price * 1 + tax);

  const calcPrice = (ev) => {
    numTickets = ev.target.value;
    setTotalPrice(ticket.price * numTickets + tax);
  };

  const saveTicket = async (e) => {
    e.preventDefault();
    try {
      const req = await fetch(`https://rythme-project.herokuapp.com/users/save_ticket`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
          "Access-Control-Allow-Origin": "*",
        },
        credentials: "include",
        body: JSON.stringify({ ticket, tax, numTickets }),
      });

      const res = await req.json();

      if (!req.ok) {
        throw new Error(res.message);
      }
      props.history.push("/ticket/buy/finish");
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="box-form">
      <GoBack />
      <div className=" box-form__header ">Compra tus entradas </div>
      <form className="form--position">
        <p className="form__title--color position">
          Entrada General: {ticket.price} €
        </p>
        <select className="form__input--size--color" onChange={calcPrice}>
          <option value="1">1 Entrada </option>
          <option value="2">2 Entradas</option>
          <option value="3">3 Entradas</option>
          <option value="4">4 Entradas</option>
        </select>
        <div className="position price__box">
          <p className="">Gastos de gestión: {tax} €</p>
          <p className="price">Precio total: {totalPrice} €</p>
        </div>
        <h3 className="form__title--color position">Datos del comprador</h3>
        <input
          className="form__input--size--color"
          type="text"
          placeholder="Nombre y Apellidos"
        />
        <input
          className="form__input--size--color"
          type="text"
          placeholder="Email"
        />
        <input
          className="form__input--size--color"
          type="number"
          placeholder="Cod.Postal"
        />
        <div className="form__button">
          <Button style="big-color" onClick={saveTicket}>
            Buy
          </Button>
        </div>
      </form>
    </div>
  );
};

export default withRouter(PayTicketPage);
