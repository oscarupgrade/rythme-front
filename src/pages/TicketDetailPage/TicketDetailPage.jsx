import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router";
import { selectTicket, selectUser, setLocationPage } from "../../app/appSlice";

import Button from "../../components/Button";
import TicketCardBig from "../../components/TicketCardBig";
import GoBack from '../../components/GoBack';
import Maps from '../../components/Maps';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMapMarkerAlt,
  faMicrophone,
  faMusic,
} from "@fortawesome/free-solid-svg-icons";

import "../../styles/mainStyles.scss";
import "./TicketDetailPage.scss";

const TicketDetailPage = (props) => {
    const dispatch = useDispatch();
    const ticket = useSelector(selectTicket);
    const user = useSelector(selectUser);

    const hasUser = () => {
        if (user?.data) {
            props.history.push('/ticket/buy');
        } else {
            dispatch(setLocationPage('login'));
            props.history.push('/login');
        }
    };

    const hasUsers = () => {
      if (user?.data) {
          props.history.push('/concerts');
      } else {
          dispatch(setLocationPage('login'));
          props.history.push('/login');
      }
  };

  return (
    
    
    <div className="detail">
    <GoBack />
      <TicketCardBig ticket={ticket} />
      <Maps/>
      <div className="detail__buttons">
        <Button onClick={hasUser} style="buy-yellow">Buy</Button>
        <Button onClick={hasUsers} type='submit' style="share">Wishlist</Button>
      </div>
      <div className="text-detail">
        <p>
          <FontAwesomeIcon icon={faMapMarkerAlt} /> {ticket?.room?.name}
        </p>
        <p>
          <FontAwesomeIcon icon={faMicrophone} /> {ticket?.singer?.name}
        </p>
        <p>
          <FontAwesomeIcon icon={faMusic} /> {ticket?.singer?.style.name}
        </p>
        <p>{ticket?.assistants} assitants</p>
        <br></br>
        <p>{ticket?.singer?.description}</p>
      </div>
    </div>
    
  );
};

export default withRouter(TicketDetailPage);
