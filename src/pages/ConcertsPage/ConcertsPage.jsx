import React, {useEffect, useState} from 'react';

import Button from "../../components/Button";
import TicketCardBig from "../../components/TicketCardBig";

import "./ConcertsPage.scss";

const ConcertsPage = () => {
   const [events, setEvents] = useState([]);
   const [wishList, setWishList] = useState([]);
   const [showConcerts, setShowConcerts] = useState(true);

  useEffect(() => {
    getData();
  },[]);

  const getData = async () => {
    const req = await fetch(`https://rythme-project.herokuapp.com/users/wishlist`, {
      method :"GET",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Credentials": true,
        "Access-Control-Allow-Origin": "*",
      },
      credentials: 'include'
    });
    const res = await req.json();

    setWishList(res.wishList);
    setEvents(res.tickets)
  };

  return (
    <div className='concerts'>
      <div className="concerts__submenu">

        <div className='concerts__position'>
          <Button onClick={() => setShowConcerts(true)} style="medium-left">Voy a ir</Button>
        </div>

        <div>
          <Button onClick={() => setShowConcerts(false)} style="medium-right">Interesado</Button></div>
        </div>

          {!showConcerts && wishList.map(ticket => {
              return(
                <>
                  <TicketCardBig  ticket={ticket} key={ticket._id}/>
                  <hr className="line"></hr>
                </>
              )
            })
          }

      {showConcerts && events.map(ticket => {
        return(
            <>
              <TicketCardBig  ticket={ticket.ticket} key={ticket._id}/>
              <hr className="line"></hr>
            </>
        )
      })
      }
    </div>
  );
};

export default ConcertsPage;
