import React from "react";
import { Link } from "react-router-dom";
import Button from "../../components/Button";

import "../../styles/mainStyles.scss";
import "./FanPage.scss";

const FanPage = () => {
  return (
    <div className="fans">
      <div className="fans__logo">
        <img
          src="https://res.cloudinary.com/dj0zfm4sd/image/upload/v1614336292/page%20logos/rithmeFanclubOk3_3x_xdroua.png"
          alt="LogoFanClub"
        />
      </div>
      <div className="fans__text">
        <h2>Bienvenido al club donde vivirás tu música. </h2>
        <p>
          RYTHME te ofrecerá aqui todo un mundo de experiencias exclusivas para
          apasionados de la música tan exigentes como tú.
        </p>
        <p>¿Te unes?</p>
      </div>
      <Link className="fans__link" to="/fans/form">
        <Button style="fanClub">Quiero unirme ahora</Button>
      </Link>
      <Link className="fans__link" to="/home">
        <p className="fans__link-green">más tarde</p>
      </Link>
    </div>
  );
};

export default FanPage;
