import { useParams } from "react-router-dom";
import ChatFriendList from "../../components/ChatFriendList";
import {useEffect, useState} from "react";

import './RoomChatPage.scss';

const RoomChatPage = () => {
    const room = useParams();
    const [msgData, setMsgData] = useState([]);

    useEffect(() => {
        getMsg();
    },[]);

    const getMsg = async () => {
        const req = await fetch(`https://rythme-project.herokuapp.com/chat/${room.room}`, {
            method :"GET",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Allow-Origin": "*",
            },
            credentials: 'include'
        });
        const res = await req.json();
        setMsgData(res.data)
    };

    return(
        <div className={'room-chat'}>
            <ChatFriendList messages={msgData} channel={room.room}/>
        </div>
    );
};

export default RoomChatPage;