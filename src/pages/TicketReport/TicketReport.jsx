import React from "react";
import { useSelector } from "react-redux";
import { selectTicket } from "../../app/appSlice";
import TicketCardBig from "../../components/TicketCardBig";
import Button from "../../components/Button";
import QRcode from '../../components/QRcodes';

import "../../styles/mainStyles.scss";
import "./TicketReport.scss";

const TicketReport = () => {
  const ticket = useSelector(selectTicket);
  return (
    <div>
      <div className="box__large">
        <TicketCardBig ticket={ticket} />
        <div className='report__text'>
          <p>{ticket.singer.name}</p>
          <p>en {ticket.room.name}</p>
          <p>{ticket.room.address}</p>
          <div className='position'>
          <QRcode />
          </div>
        </div>
      </div>

      

      <div className="report__button">
        <Button style="color">Guardar en PDF</Button>
      </div>
    </div>
  );
};

export default TicketReport;
