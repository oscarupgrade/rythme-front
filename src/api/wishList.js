const wishListUrl = `https://rythme-project.herokuapp.com/users/wishlist`;

export const wishPost = async (wishData) => {
    const request = await fetch(wishListUrl, {
        method :"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        body: JSON.stringify(wishData),
    });

    const response = await request.json();

     if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
};

export const wishGet = async () => {
    const request = await fetch(wishListUrl, {
        method :"GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            // "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        // body: JSON.stringify(),
    });

    const response = await request.json();

     if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
};