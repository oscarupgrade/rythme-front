import React, { useEffect } from 'react';
import { useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { setUser } from "./app/appSlice";
import { checkSession } from './api/auth';

import MainMenu from './components/MainMenu';
import Header from './components/Header';
import Register from './components/Register';
import Login from './components/Login';
import FinishBuy from './components/FinishBuy';
import Platforms from './components/Platforms';
import HomePage from './pages/HomePage';
import TicketReport from './pages/TicketReport';
import TicketsPage from './pages/TicketsPage';
import SearchPage from './pages/SearchPage';
import ConcertsPage from './pages/ConcertsPage';
import TicketDetailPage from './pages/TicketDetailPage';
import ProfilePage from './pages/ProfilePage';
import PayTicketPage from './pages/PayTicketPage';
import LoadingPage from './pages/LoadingPage';
import FanPage from './pages/FanPage';
import FanForm from './components/FanForm';
import ChatPage from "./pages/ChatPage";
import RoomChatPage from "./pages/RoomChatPage";
// import EditProfilePage from './pages/EditProfilePage';
import MusicPage from './pages/musicPage';

import './styles/mainStyles.scss';
import './App.scss';

const App = (props) => {
    const dispatch = useDispatch();

    useEffect(() => {
        checkUserSession()
        }, []);

    const checkUserSession = async () => {
        try {
          const data = await checkSession();
          delete data.password;
          dispatch(setUser(data));
        } catch (error) {
            dispatch(setUser(null));
        }
    };

    return(
      <Router>
      <Header />
          <Switch>
              <Route path={'/'} exact component={LoadingPage} />
              <Route path={'/home'} exact component={HomePage}  />
              <Route path={'/register'} exact component={Register}/>
              <Route path={'/login'} exact component={Login}/>
              <Route path={'/tickets'} exact component={TicketsPage} />
              <Route path={'/search'} exact component={SearchPage} />
              <Route path={'/concerts'} exact component={ConcertsPage} />
              <Route path={'/profile'} exact component={ProfilePage} />
              <Route path={'/ticket/buy'} exact component={PayTicketPage} />
              <Route path={'/ticket/buy/finish'} exact component={FinishBuy} />
              <Route path={'/ticket/report'} exact component={TicketReport} />
              <Route path={'/ticket/detail'} exact component={TicketDetailPage} />
              <Route path={'/sync/platforms'} exact component={Platforms} />
              <Route path={'/fans/form'} exact component={FanForm} />
              <Route path={'/fans'} exact component={FanPage} />
              <Route path={'/sync/platforms/music'} exac component={MusicPage}/>
              <Route path={'/chat'} exact component={ChatPage} />
              <Route path={'/room-chat/:room'} exact component={RoomChatPage} />
          </Switch>
        <MainMenu {...props}/>
      </Router>
    )
};

export default App;
