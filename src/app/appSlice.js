import { createSlice } from "@reduxjs/toolkit";
import {
    setUserReducer,
    setLocationPageReducer,
    setTicketReducer,
    setLoadingReducer,
    setRoomReducer
} from "./appReducers";

export const appSlice = createSlice( {
    name: 'app',
    initialState: {
        user: { data: null, message: null },
        locationPage: 'home',
        ticket: {},
        loading: true,
        room: '1'
    },
    reducers: {
        setUser: setUserReducer,
        setLocationPage: setLocationPageReducer,
        setTicket: setTicketReducer,
        setLoading: setLoadingReducer,
        setRoom: setRoomReducer
    }
                                     });

export const {
    setUser,
    setLocationPage,
    setHeader,
    setTicket,
    setLoading,
    setRoom
} = appSlice.actions;

export const selectUser = (state) => state.app.user;
export const selectLocation = (state) => state.app.locationPage;
export const selectTicket = (state) => state.app.ticket;
export const selectLoading = (state) => state.app.loading;
export const selectRoom = (state) => state.app.room;

export default appSlice.reducer;