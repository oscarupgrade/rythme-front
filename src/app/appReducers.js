export const setUserReducer = (state, action) => {
    state.user = action.payload;
};

export const setLocationPageReducer = (state, action) => {
    state.locationPage = action.payload;
};

export const setTicketReducer = (state, action ) => {
    state.ticket = action.payload;
};

export const setLoadingReducer = (state, action) => {
    state.loading = action.payload;
};

export const setRoomReducer = (state, action) => {
    state.room = action.payload;
};